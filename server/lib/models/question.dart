import 'package:json_annotation/json_annotation.dart';

part 'question.g.dart';

@JsonSerializable(explicitToJson: true)
class Question {
  Question({
    required this.question,
    required this.possibleAnswers,
    required this.answer,
  });

  factory Question.fromJson(Map<String, dynamic> json) =>
      _$QuestionFromJson(json);

  String question;
  List<String> possibleAnswers;
  String answer;

  Map<String, dynamic> toJson() => _$QuestionToJson(this);
}
