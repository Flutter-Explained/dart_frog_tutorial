import 'package:json_annotation/json_annotation.dart';
import 'package:server/models/question.dart';

part 'questions.g.dart';

@JsonSerializable(explicitToJson: true)
class Questions {
  Questions({required this.questions});

  factory Questions.fromJson(Map<String, dynamic> json) =>
      _$QuestionsFromJson(json);

  List<Question> questions;

  Map<String, dynamic> toJson() => _$QuestionsToJson(this);
}
