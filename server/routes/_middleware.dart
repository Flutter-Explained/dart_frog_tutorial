import 'dart:convert';
import 'dart:io';

import 'package:dart_frog/dart_frog.dart';
import 'package:server/models/question.dart';
import 'package:server/models/questions.dart';

List<Question>? _questions;

Middleware cachedQuestionsProvider() {
  return provider<Future<List<Question>>>((context) async {
    if (_questions != null) return Future.value(_questions);
    final rawJSON = await File('quizzes.json').readAsString();
    final decode = json.decode(rawJSON);
    final decodedJson = Questions.fromJson(decode as Map<String, dynamic>);
    return _questions ??= decodedJson.questions;
  });
}

Handler middleware(Handler handler) {
  return handler.use(cachedQuestionsProvider());
}
