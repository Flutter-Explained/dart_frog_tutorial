import 'package:dart_frog/dart_frog.dart';
import 'package:server/models/question.dart';

Future<Response> onRequest(
  RequestContext context,
  String questionNumber,
) async {
  final requestQuestion = int.parse(questionNumber);
  final allQuestions = await context.read<Future<List<Question>>>();
  final responseQuestion = allQuestions[requestQuestion];

  return Response.json(
    body: <String, dynamic>{
      'question': responseQuestion.question,
      'possibleAnswers': responseQuestion.possibleAnswers
    },
  );
}
