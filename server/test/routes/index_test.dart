import 'dart:io';

import 'package:test/test.dart';

import '../../routes/index.dart' as route;
import '../utils/mockRequestContext.dart';

void main() {
  group('GET /', () {
    test('responds with a 200 and "Welcome to Dart Frog!".', () {
      final context = MockRequestContext();
      final response = route.onRequest(context);
      expect(response.statusCode, equals(HttpStatus.ok));
      expect(
        response.body(),
        completion(equals('Welcome to Dart Frog!')),
      );
    });
  });
}
