import 'dart:convert';
import 'dart:io';

import 'package:dart_frog/dart_frog.dart';
import 'package:mocktail/mocktail.dart';
import 'package:server/models/question.dart';

import 'package:test/test.dart';

import '../../../routes/api/answers/[questionNumber].dart' as route;
import '../../utils/mockRequestContext.dart';

void main() {
  late RequestContext context;

  setUp(() {
    context = MockRequestContext();
  });

  group('GET /api/answers/[questionNumber]', () {
    test("responds with 200 and '3.3.0' ", () async {
      final questions = [
        Question(
          answer: '42',
          possibleAnswers: ['42', '32', '99'],
          question: 'Test Question',
        )
      ];

      when(() => context.read<Future<List<Question>>>())
          .thenAnswer((_) async => questions);

      final response = await route.onRequest(context, '0');
      expect(response.statusCode, equals(HttpStatus.ok));

      final rawJson = jsonEncode(await response.json());
      expect(rawJson, stringContainsInOrder(['answer', '42']));
    });

    test("responds with 500 WHEN no int is provided", () async {
      final questions = [
        Question(
          answer: '42',
          possibleAnswers: ['42', '32', '99'],
          question: 'Test Question',
        )
      ];

      when(() => context.read<Future<List<Question>>>())
          .thenAnswer((_) async => questions);

      expect(
        () async => route.onRequest(context, 'Tissue'),
        throwsFormatException,
      );
    });
  });
}
