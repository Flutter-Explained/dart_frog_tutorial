import 'package:client/bloc/model/question.dart';
import 'package:client/bloc/quiz_service.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  QuizService service = QuizService();
  String? answer;
  bool answerCorrect = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Quiz App"),
      ),
      body: FutureBuilder<Question>(
        future: service.getQuestion(),
        builder: (context, AsyncSnapshot<Question> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                snapshot.error.toString(),
              ),
            );
          }

          if (snapshot.hasData && snapshot.data != null) {
            Question question = snapshot.data!;
            return Column(
              children: [
                Expanded(
                  child: GridView.count(
                    crossAxisSpacing: 8,
                    mainAxisSpacing: 8,
                    crossAxisCount: 2,
                    children: [
                      for (final q in question.possibleAnswers)
                        ElevatedButton(
                          onPressed: () {
                            answer = q;
                          },
                          child: Center(child: Text(q)),
                        )
                    ],
                  ),
                ),
                Icon(
                  answerCorrect ? Icons.check_outlined : Icons.cancel_outlined,
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        onPressed: () async {
                          if (answer == null) return;
                          answerCorrect = await service.answerQuestion(answer!);
                          setState(() {});
                        },
                        child: const Text("Submit Answer"),
                      ),
                    ],
                  ),
                )
              ],
            );
          }

          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
