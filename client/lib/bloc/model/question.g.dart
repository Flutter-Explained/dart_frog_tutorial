// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'question.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Question _$QuestionFromJson(Map<String, dynamic> json) => Question(
      possibleAnswers: (json['possibleAnswers'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      question: json['question'] as String,
    );

Map<String, dynamic> _$QuestionToJson(Question instance) => <String, dynamic>{
      'possibleAnswers': instance.possibleAnswers,
      'question': instance.question,
    };
