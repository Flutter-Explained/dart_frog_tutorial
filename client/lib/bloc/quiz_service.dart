import 'dart:convert';

import 'package:client/bloc/model/answer.dart';
import 'package:client/bloc/model/question.dart';
import 'package:http/http.dart' as http;

class QuizService {
  Future<Question> getQuestion() async {
    var url = "http://localhost:8080/api/questions/0";
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      String rawJson = response.body;
      return Question.fromJson(jsonDecode(rawJson));
    }
    throw UnimplementedError();
  }

  Future<bool> answerQuestion(String answer) async {
    var url = "http://localhost:8080/api/answers/0";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      String rawJson = response.body;
      final result = Answer.fromJson(jsonDecode(rawJson));
      return result.answer == answer;
    }
    throw UnimplementedError();
  }
}
